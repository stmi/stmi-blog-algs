package pl.stmi.algs;

import net.jqwik.api.*;
import net.jqwik.api.constraints.Size;
import net.jqwik.api.lifecycle.BeforeTry;
import net.jqwik.api.statistics.Statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static java.util.Collections.emptyIterator;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class NWayMergeTest {

    private NWayMerge<Integer> merge;
    private List<Integer> actual;

    @BeforeTry
    public void beforeTry() {
        actual = new ArrayList<>();
    }

    @Example
    void singleIterator() {
        merge = new NWayMerge<>(of(
                of(1, 2, 3, 4, 5).iterator()
        ));

        while (merge.hasNext()) {
            actual.add(merge.next());
        }

        assertThat(actual).isEqualTo(of(1, 2, 3, 4, 5));
    }

    @Provide
    Arbitrary<List<Iterator<Integer>>> edgeInputs() {
        return Arbitraries.of(
                null,
                List.of(),
                List.of(emptyIterator()));
    }

    @Property
    @Label("Edge cases for input")
    void edgeCase(@ForAll("edgeInputs") List<Iterator<Integer>> input) {
        merge = new NWayMerge<>(input);

        assertThat(merge.hasNext()).isFalse();
        assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(() -> merge.next());
    }

    // Instead of implementing custom generation to guarantee sorted lists just sort random lists
    @Property(tries = 5000)
    @Label("Random iterator inputs")
    void mergeRandomSortedLists(
            @ForAll @Size(max = 20) List<@Size(max = 50) List<Integer>> inputs) {
        inputs.forEach(Collections::sort);
        int expectedSize = inputs.stream().mapToInt(List::size).sum();

        List<Iterator<Integer>> iterators = inputs.stream().map(List::iterator).collect(Collectors.toList());

        merge = new NWayMerge<>(iterators);
        while (merge.hasNext()) {
            actual.add(merge.next());
        }

        assertThat(actual).isSorted();
        assertThat(actual.size()).isEqualTo(expectedSize);

        Statistics.label("Inputs size").collect(inputs.size());
        Statistics.label("Single input size min").collect(inputs.stream().mapToInt(List::size).min().orElse(0));
        Statistics.label("Single input size max").collect(inputs.stream().mapToInt(List::size).max().orElse(0));
    }
}
