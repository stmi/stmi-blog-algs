package pl.stmi.algs.notes;

import net.jqwik.api.Example;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import pl.stmi.algs.data.Tree;
import pl.stmi.algs.tree.*;

import java.security.SecureRandom;
import java.util.*;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

class RandomTreeRemyTest {

    private final Trimmer<Integer> trimmer = new Trimmer<>();
    private final TreeSize treeSize = new TreeSize();
    private final Random random = new Random();

    private final WeightSequence weightSequence = new WeightSequence();
    private FactoradicConverter converter = new FactoradicConverter();

    private final RandomTreeRemy<Integer> generator = new RandomTreeRemy<>(random);

    @Property
    void treeGeneratedWithExpectedSize(@ForAll @IntRange(min = 0, max = 65536) int internalNodes) {
        Integer actual = generator.andThen(trimmer).andThen(treeSize).apply(internalNodes);

        assertThat(actual).isEqualTo(internalNodes);
    }

    // NOTE: Catalan numbers array is filled in correctly starting from idx = 0,
    // while test will be using values from 1
    private static final int[] catalan = new int[]{1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862, 16796, 58786, 208012, 742900, 2674440, 9694845};

    @Timeout(value = 60, unit = SECONDS)
    @Property
    void allTreeShapesArePossibleForGivenSize(@ForAll @IntRange(min = 1, max = 13) int treeSize) {
        Set<Long> distinctShapes = new TreeSet<>();

        int count = catalan[treeSize];

        while (distinctShapes.size() < count) {
            Tree<Integer> tree = generator.andThen(trimmer).apply(treeSize);

            long treeId =
                    weightSequence
                    .andThen(converter)
                    .apply(tree);

            assertThat(this.treeSize.apply(tree)).isEqualTo(treeSize);

            distinctShapes.add(treeId);
        }

        assertThat(distinctShapes.size()).isEqualTo(count);
    }

    @Test
    void resultsFollowUniformDistribution() {
        Map<Long, Long> frequencyCounters = new TreeMap<>();

        ChiSquareTest chiSquareTest = new ChiSquareTest();

        for (int i = 0; i < 50000; i++) {
            long treeId = generator
                    .andThen(trimmer)
                    .andThen(weightSequence)
                    .andThen(converter)
                    .apply(5);

            frequencyCounters.computeIfPresent(treeId, (key, val) -> val + 1);
            frequencyCounters.putIfAbsent(treeId, 1L);
        }

        System.out.println(frequencyCounters.values().stream().sorted().toList());

        long[] observed = frequencyCounters.values().stream().limit(14).mapToLong(a -> a).toArray();
        double[] expected = Collections.nCopies(14, 7143.0d).stream().mapToDouble(a -> a).toArray();

        double alpha = 0.01; // confidence level 99%

        double chiSquare = chiSquareTest.chiSquare(expected, observed);
        System.out.println("ChiSquare = " + chiSquare);

        boolean pass = chiSquareTest.chiSquareTest(expected, observed, alpha);
        System.out.println("Pass = " + pass);
    }

    @Test
    void randomQualityIsOk() {
        Map<Integer, Long> frequencyCounters = new TreeMap<>();

        ChiSquareTest chiSquareTest = new ChiSquareTest();

        for (int i = 0; i < 20000; i++) {
            int nextInt = secureRandom.nextInt(20);

            frequencyCounters.computeIfPresent(nextInt, (key, val) -> val + 1);
            frequencyCounters.putIfAbsent(nextInt, 1L);
        }

        System.out.println(frequencyCounters.values().stream().sorted().toList());

        long[] observed = frequencyCounters.values().stream().mapToLong(a -> a).toArray();
        double[] expected = Collections.nCopies(20, 1000.0d).stream().mapToDouble(a -> a).toArray();

        double alpha = 0.01; // confidence level 99%

        double pval = chiSquareTest.chiSquare(expected, observed);
        System.out.println("Chi Square V = " + pval);

        double pass = chiSquareTest.chiSquareTest(expected, observed);
        System.out.println("P-value = " + pass);
    }

    @Test
    void integerSeqence() {
        // 1, 1, 2, 5, 14, 42, 132, 429

        System.out.println(distinctShapes(1,1).stream().sorted().toList());
        System.out.println(distinctShapes(2,2).stream().sorted().toList());

        System.out.println(distinctShapes(3,5).stream().sorted().toList());
        System.out.println(distinctShapes(4,14).stream().sorted().toList());

        System.out.println(distinctShapes(5,42).stream().sorted().toList());
        System.out.println(distinctShapes(6,132).stream().sorted().toList());

    }

    Set<Long> distinctShapes(int n, int limit) {
        Set<Long> distinctShapes = new HashSet<>();

        while (distinctShapes.size() < limit) {
            long treeId = generator
                    .andThen(trimmer)
                    .andThen(weightSequence)
                    .andThen(converter)
                    .apply(n);

            distinctShapes.add(treeId);
        }

        return distinctShapes;
    }
}