package pl.stmi.algs.notes;

import net.jqwik.api.Example;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import org.apache.commons.math3.util.CombinatoricsUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Convention assumed is to write factoradic numbers starting with factorial sign !
 * and not to include trailing 0 with 0! which is always equal to 0 anyway
 *
 * examples:
 *
 * !0 = 0
 * !1 = 1
 * !1:0 = 2
 * !1:1 = 3 = 1 * 2! + 1 * 1!
 * !2:0 = 4 = 2 * 2! + 0 * 1!
 *
 */
class FactoradicConverterTest {

    FactoradicConverter converter = new FactoradicConverter();

    @Example
    void zeroConversion() {
        Long expected = 0L;
        Long actual = converter.apply(List.of(0));

        assertThat(actual).isEqualTo(expected);
    }

    @Example
    void oneConversion() {
        Long expected = 1L;
        Long actual = converter.apply(List.of(1));

        assertThat(actual).isEqualTo(expected);
    }

    @Example
    void twoConversion() {
        Long expected = 2L;
        Long actual = converter.apply(List.of(0,1));

        assertThat(actual).isEqualTo(expected);
    }

    @Example
    void threeConversion() {
        Long expected = 3L;
        Long actual = converter.apply(List.of(1,1));

        assertThat(actual).isEqualTo(expected);
    }

    @Property
    void sumsToNext(@ForAll @IntRange(min = 1, max = 19) int num) {
        long factorial = CombinatoricsUtils.factorial(num + 1);

        List<Integer> factoradic = IntStream.rangeClosed(1, num)
                .boxed()
                .toList();

        assertThat(converter.apply(factoradic) + 1).isEqualTo(factorial);
    }


}