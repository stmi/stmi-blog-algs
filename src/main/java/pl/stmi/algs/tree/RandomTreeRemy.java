package pl.stmi.algs.tree;

import lombok.RequiredArgsConstructor;
import pl.stmi.algs.data.Tree;

import java.util.Random;
import java.util.function.Function;
import java.util.function.IntFunction;

/**
 * Implementation of Remy algorithm for Binary Tree Generation.
 * <p>
 * This algorithm is only concerned with shape of the tree, no values are filled in for keys.
 * <p>
 * Based on paper by Erkki Mäkinen and Jarmo Siltaneva
 * "A Note on Rémy's Algorithm for Generating Random Binary Trees"
 * <p>
 * Available in OpenAccess on ProjectEuclid
 * https://projecteuclid.org/euclid.mjms/1567216867
 * <p>
 * Original paper by Jean-Luc Rémy
 * "Un procédé itératif de dénombrement d’arbres binaires et son application à leur génération aléatoire"
 * <p>
 * It's in French and I don't know French, but I'm mentioning it for sake of information.
 * <p>
 * Available in OpenAccess on Numdam, the French digital mathematics library
 * http://www.numdam.org/item/?id=ITA_1985__19_2_179_0
 */
@RequiredArgsConstructor
public class RandomTreeRemy<T> implements IntFunction<Tree<T>>, Function<Integer, Tree<T>> {
    private final Random random;

    public RandomTreeRemy() {
        this.random = new Random();
    }

    /**
     * Generates a strictly binary tree with N internalNodes (2N + 1 total).
     * <p>
     * Can be used to generate a unary-binary tree with N nodes by trimming leaves.
     */
    @SuppressWarnings("unchecked")
    @Override
    public Tree<T> apply(int internalNodes) {
        if (internalNodes < 0) {
            throw new IllegalArgumentException();
        }

        int limit = 2 * internalNodes + 1;
        Tree<T>[] nodes;
        nodes = new Tree[limit];

        // Initialize
        nodes[0] = new Tree<>();

        for (int i = 1; i < limit; i += 2) {
            int hit = random.nextInt(i);
            boolean direction = random.nextBoolean();

            Tree<T> root = nodes[hit];

            Tree<T> tree = new Tree<>();
            Tree<T> leaf = new Tree<>();

            nodes[i] = tree;
            nodes[i + 1] = leaf;

            tree.left(root.left());
            tree.right(root.right());

            if (direction) {
                root.left(tree);
                root.right(leaf);
            } else {
                root.left(leaf);
                root.right(tree);
            }
        }

        return nodes[0];
    }

    @Override
    public Tree<T> apply(Integer integer) {
        if (integer == null) {
            throw new NullPointerException();
        }

        return apply(integer.intValue());
    }
}
