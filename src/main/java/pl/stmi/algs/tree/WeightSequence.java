package pl.stmi.algs.tree;

import pl.stmi.algs.data.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class WeightSequence implements Function<Tree<?>, List<Integer>> {

    @Override
    public List<Integer> apply(Tree<?> tree) {
        List<Integer> sequence = new ArrayList<>();

        recursive(tree, sequence);

        return sequence;
    }

    private int recursive(Tree<?> root, List<Integer> sequence) {
        if (root == null) {
            return 1;
        }

        int leftCount = recursive(root.left(), sequence);
        sequence.add(leftCount);
        int rightCount = recursive(root.right(), sequence);

        return leftCount + rightCount;
    }
}
