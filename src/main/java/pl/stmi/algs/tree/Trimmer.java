package pl.stmi.algs.tree;

import pl.stmi.algs.data.Tree;

import java.util.function.UnaryOperator;

public class Trimmer<K> implements UnaryOperator<Tree<K>> {

    @Override
    public Tree<K> apply(Tree<K> tree) {
        if (tree == null) {
            return null;
        }

        if (tree.left() == null && tree.right() == null) {
            return null;
        }

        tree.left(apply(tree.left()));
        tree.right(apply(tree.right()));

        return tree;
    }
}
