package pl.stmi.algs.tree;

import pl.stmi.algs.data.Tree;

import java.util.function.Function;
import java.util.function.ToIntFunction;

public class TreeSize implements ToIntFunction<Tree<?>>, Function<Tree<?>, Integer> {

    @Override
    public int applyAsInt(Tree<?> value) {
        if (value == null) {
            return 0;
        }

        return applyAsInt(value.left()) + applyAsInt(value.right()) + 1;
    }

    @Override
    public Integer apply(Tree<?> tree) {
        return applyAsInt(tree);
    }
}
