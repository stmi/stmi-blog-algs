package pl.stmi.algs.tree;

import lombok.RequiredArgsConstructor;
import pl.stmi.algs.data.Tree;

import java.util.Random;

/**
 * Draft attempt at Boltzmann sampler for Binary-Unary Trees
 *
 * Based on post
 * https://byorgey.wordpress.com/2013/04/25/random-binary-trees-with-a-size-limited-critical-boltzmann-sampler-2/
 */
@RequiredArgsConstructor
public class RandomTreeBoltzmann {

    private final Random random;

    private static final double TOLERANCE = 0.1d;
    private static final int MIN_TOLERANCE = 2;

    @RequiredArgsConstructor
    private static class State {
        final int lowerBound;
        final int upperBound;

        int currSize = 0;
    }

    public Tree<Integer> generate(int size) {
        // Establish target range
        int margin = Math.max((int) (size * TOLERANCE), MIN_TOLERANCE);

        int lowerBound = Math.max(size - margin, 1);
        int upperBound = size + margin;

        State state = new State(lowerBound, upperBound);

        return generateLowerBound(state);
    }

    private Tree<Integer> generateLowerBound(State state) {
        while (true) {
            try {
                state.currSize = 0;
                Tree<Integer> tree = genTreeUpperBound(state);
                if (state.currSize >= state.lowerBound) {
                    return tree;
                } // else try again in next iteration
            } catch (IllegalStateException ignored) {
            }
        }
    }

    private Tree<Integer> genTreeUpperBound(State state) {
        state.currSize++;

        if (state.currSize > state.upperBound) {
            throw new IllegalStateException();
        }

        double threshold = random.nextDouble();
        int val = random.nextInt();

           if (threshold <= 0.5d) {
               return new Tree<>(val, null, null);
           } else {
               Tree<Integer> left = genTreeUpperBound(state);
               Tree<Integer> right = genTreeUpperBound(state);

               return new Tree<>(val, left, right);
           }
    }
}
