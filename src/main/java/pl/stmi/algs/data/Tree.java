package pl.stmi.algs.data;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Tree<K> {
    private K key;
    private Tree<K> left;
    private Tree<K> right;
}
