package pl.stmi.algs.notes;


import com.google.common.collect.Streams;
import pl.stmi.algs.data.Tree;
import pl.stmi.algs.draw.ReingoldTilfordTreeDraw;
import pl.stmi.algs.tree.RandomTreeRemy;

import pl.stmi.algs.tree.Trimmer;
import pl.stmi.algs.tree.WeightSequence;

import java.util.*;

public class TreeHtmlTable {

    record TableRow(long label, List<Integer> weightSequence, String treeSvg) {
    }

    public static void main(String[] args) {
        Trimmer<Integer> trimmer = new Trimmer<>();
        Random random = new Random();

        WeightSequence weightSequence = new WeightSequence();
        FactoradicConverter converter = new FactoradicConverter();

        RandomTreeRemy<Integer> generator = new RandomTreeRemy<>(random);
        Map<Long, TableRow> distinctShapes = new TreeMap<>();

        while (distinctShapes.size() < 42) {
            Tree<Integer> tree = generator.andThen(trimmer).apply(5);

            String treeSvg = ReingoldTilfordTreeDraw.drawTreeSvg(tree);

            List<Integer> sequence = weightSequence.apply(tree);

            Long factoradic = converter.apply(sequence);

            distinctShapes.putIfAbsent(factoradic, new TableRow(factoradic, sequence, treeSvg));
        }

        System.out.println("<table>");
        distinctShapes.forEach((k, v) -> {
            String row = """
                    <tr>
                      <th>%s</th>
                      <th>%s</th>
                      <th>%s</th>
                    </tr>
                              """.formatted(v.label, v.treeSvg, v.weightSequence);
            System.out.println(row);
        });
        System.out.println("</table>");
    }
}
