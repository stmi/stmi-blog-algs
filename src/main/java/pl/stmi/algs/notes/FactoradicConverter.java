package pl.stmi.algs.notes;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToLongFunction;

/**
 * Class for converting factoradic numbers
 */
public class FactoradicConverter implements ToLongFunction<List<Integer>>, Function<List<Integer>, Long> {

    @Override
    public Long apply(List<Integer> factoradic) {
        if (factoradic.size() > 19) throw new ArithmeticException();
        if (factoradic.stream().anyMatch(integer -> integer < 0)) throw new ArithmeticException();

        long result = 0L;

        long base = 1L;

        for (int i = 0; i < factoradic.size(); i++) {
            base *= (i+1);

            int curr = factoradic.get(i);
            if (curr > i + 1) throw new ArithmeticException("Wrong factoradic number");

            result += base * curr;
        }

        return result;
    }

    @Override
    public long applyAsLong(List<Integer> value) {
        return 0;
    }
}
