package pl.stmi.algs.draw;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class TreeNode {
    int x, y;
    int offset;
    TreeNode left, right;
    boolean thread;

    public static SetupState newState(TreeNode tree) {
        return new SetupState(tree);
    }
}
