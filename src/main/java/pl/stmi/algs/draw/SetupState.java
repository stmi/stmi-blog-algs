package pl.stmi.algs.draw;

import lombok.*;

@Getter
@Setter
@ToString
public class SetupState {

    public SetupState(TreeNode tree) {
        this.tree = tree;
        this.lMost.addr(tree);
        this.rMost.addr(tree);
    }

    private TreeNode tree;
    private Extreme rMost = new Extreme();
    private Extreme lMost = new Extreme();
}

