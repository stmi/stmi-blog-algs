package pl.stmi.algs.draw;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.stmi.algs.draw.TreeNode;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Extreme {
    TreeNode addr;
    int offset;
    int level;
}
