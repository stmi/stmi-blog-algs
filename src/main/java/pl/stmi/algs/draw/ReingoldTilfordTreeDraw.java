package pl.stmi.algs.draw;

import org.jfree.svg.SVGGraphics2D;
import org.jfree.svg.SVGUtils;
import pl.stmi.algs.data.Tree;
import pl.stmi.algs.tree.RandomTreeRemy;
import pl.stmi.algs.tree.Trimmer;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import static java.lang.Math.abs;
import static pl.stmi.algs.draw.TreeNode.newState;

public class ReingoldTilfordTreeDraw {

    public static final int MIN_SEP = 1;

    public static final int STEP = 20;
    public static final int MARGIN = STEP / 2 + 1;
    public static final int RAD = STEP / 4;

    public static void main(String[] args) throws IOException {
        long seed = System.currentTimeMillis();
//        long seed = 1610307526360L;
        System.out.println(seed);

        Trimmer<Integer> trimmer = new Trimmer<>();

        Random random = new Random(seed);
        RandomTreeRemy<Integer> remy = new RandomTreeRemy<>(random);

        Tree<Integer> withLeaves = remy.apply(5);
        Tree<Integer> tree = trimmer.apply(withLeaves);

        TreeNode node = TreeMapper.map(tree);

        setup(newState(node), 0);

        petrify(node, 0);

        int offset = scanLeft(node);

        shift(node, abs(offset));

        int right = scanRight(node);
        int height = scanHeight(node);

        String raster = raster(node, right + 1, height + 1);

        SVGUtils.writeToSVG(new File("tree.svg"), raster);
    }

    public static String drawTreeSvg(Tree<?> tree) {
        TreeNode node = TreeMapper.map(tree);

        setup(newState(node), 0);

        petrify(node, 0);

        int offset = scanLeft(node);

        shift(node, abs(offset));

        int right = scanRight(node);
        int height = scanHeight(node);

        return raster(node, right + 1, height + 1);
    }

    private static String raster(TreeNode node, int right, int height) {
        int graphicsWidth = right * STEP + 2 * MARGIN;
        int graphicsHeight = height * STEP + 2 * MARGIN;

        SVGGraphics2D g2 = new SVGGraphics2D(graphicsWidth, graphicsHeight);

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, graphicsWidth, graphicsHeight);

        g2.setPaint(Color.BLACK);
        g2.setStroke(new BasicStroke((float) STEP/8));

        rasterVisit(node, g2);

        return g2.getSVGElement();
    }

    private static void ovalAtPoint(SVGGraphics2D g2, Point start) {
        g2.fillOval(start.x - RAD, start.y - RAD, RAD * 2, RAD * 2);
    }

    private static Point pointAtCell(int x, int y) {
        int xOut = 2 * MARGIN + x * STEP;
        int yOut = 2 * MARGIN + y * STEP;
        return new Point(xOut, yOut);
    }

    private static void rasterVisit(TreeNode tree, SVGGraphics2D g2) {
        if (tree == null) {
            return;
        }

        TreeNode left = tree.left();
        TreeNode right = tree.right();

        Point rootPoint = pointAtCell(tree.x(), tree.y());
        ovalAtPoint(g2, rootPoint);

        if (left != null) {
            Point leftPoint = pointAtCell(left.x(), left.y());
            connectPoints(rootPoint, leftPoint, g2);
            rasterVisit(left, g2);
        }

        if (right != null) {
            Point rightPoint = pointAtCell(right.x(), right.y());
            connectPoints(rootPoint, rightPoint, g2);
            rasterVisit(right, g2);
        }
    }

    private static void connectPoints(Point src, Point dest, SVGGraphics2D g2) {
        g2.drawLine(src.x, src.y, dest.x, dest.y);
    }

    private static int scanHeight(TreeNode tree) {
        TreeNode left = tree.left();
        TreeNode right = tree.right();

        if (left != null && right != null) {
            return Math.max(scanHeight(left), scanHeight(right));
        } else if (left != null) {
            return Math.max(tree.y(), scanHeight(left));
        } else if (right != null) {
            return Math.max(tree.y(), scanHeight(right));
        } else {
            return tree.y();
        }
    }

    private static int scanRight(TreeNode tree) {
        TreeNode left = tree.left();
        TreeNode right = tree.right();

        if (left != null && right != null) {
            return Math.max(scanRight(left), scanRight(right));
        } else if (left != null) {
            return Math.max(tree.x(), scanRight(left));
        } else if (right != null) {
            return Math.max(tree.x(), scanRight(right));
        } else {
            return tree.x();
        }
    }

    private static void shift(TreeNode tree, int offset) {
        if (tree == null) {
            return;
        }

        tree.x(tree.x() + offset);

        shift(tree.left(), offset);
        shift(tree.right(), offset);
    }

    private static int scanLeft(TreeNode tree) {
        TreeNode left = tree.left();
        TreeNode right = tree.right();

        if (left != null && right != null) {
            return Math.min(scanLeft(left), scanLeft(right));
        } else if (left != null) {
            return Math.min(tree.x(), scanLeft(left));
        } else if (right != null) {
            return Math.min(tree.x(), scanLeft(right));
        } else {
            return tree.x();
        }
    }

    public static SetupState setup(SetupState state, int level) {
        TreeNode tree = state.tree();

        if (tree == null) {
            state.lMost().level(-1);
            state.rMost().level(-1);
            return state;
        }

        tree.y(level);
        TreeNode left = tree.left(), right = tree.right();

        if (left == null && right == null) {
            state.lMost().addr(tree);
            state.rMost().addr(tree);
            state.lMost().level(level);
            state.rMost().level(level);
            state.lMost().offset(0);
            state.rMost().level(0);

            tree.offset(0);

            return state;
        }

        int currSep, rootSep;
        int lOffSum = 0, rOffSum = 0;

        SetupState leftState = setup(newState(left), level + 1);
        SetupState rightState = setup(newState(right), level + 1);

        Extreme lr = leftState.rMost();
        Extreme ll = leftState.lMost();
        Extreme rr = rightState.rMost();
        Extreme rl = rightState.lMost();

        currSep = MIN_SEP;
        rootSep = MIN_SEP;

        while (left != null && right != null) {
            // Push if needed
            if (currSep < MIN_SEP) {
                rootSep = rootSep + (MIN_SEP - currSep);
                currSep = MIN_SEP;
            }

            // Advance pointers
            if (left.right() != null) {
                lOffSum += left.offset();
                currSep -= left.offset();
                left = left.right();
            } else {
                lOffSum -= left.offset();
                currSep += left.offset();
                left = left.left();
            }

            if (right.left() != null) {
                rOffSum -= right.offset();
                currSep -= right.offset();
                right = right.left();
            } else {
                rOffSum += right.offset();
                currSep += right.offset();
                right = right.right();
            }
        }

        // Set offset in root and include it in accumulated offsets for left and right
        tree.offset( (rootSep + 1) / 2);
        lOffSum -= tree.offset();
        rOffSum += tree.offset();

        //Update extreme nodes information
        if (rl.level() > ll.level() || tree.left() == null) {
            // Equivalent of Pascal Var parameter overwrite, needs to be changed somehow later
            state.lMost(rl);
            state.lMost().offset(state.lMost().offset() + tree.offset());
        } else {
            state.lMost(ll);
            state.lMost().offset(state.lMost().offset() - tree.offset());
        }

        if (lr.level() > rr.level() || tree.right() == null) {
            // Equivalent of Pascal Var parameter overwrite, needs to be changed somehow later
            state.rMost(lr);
            state.rMost().offset(state.rMost().offset() - tree.offset());
        } else {
            state.rMost(rr);
            state.rMost().offset(state.rMost().offset() - tree.offset());
        }

        if (left != null && left != tree.left()) {
            rr.addr().thread(true);
            int rrAddrOffset = abs((rr.offset() + tree.offset()) - lOffSum);
            rr.addr().offset(rrAddrOffset);
            if (lOffSum - tree.offset() <= rr.offset()) {
                rr.addr().left(left);
            } else {
                rr.addr().right(left);
            }
        } else if (right != null && right != tree.right()) {
            ll.addr().thread(true);
            int llAddrOffset = abs((ll.offset() - tree.offset()) - rOffSum);
            ll.addr().offset(llAddrOffset);
            if (rOffSum + tree.offset() >= ll.offset()) {
                ll.addr().right(right);
            } else {
                ll.addr().left(right);
            }
        }
        return state;
    }

    public static void petrify(TreeNode tree, int xPos) {
        if (tree == null) {
            return;
        }

        tree.x(xPos);
        if (tree.thread()) {
            tree.thread(false);
            tree.left(null);
            tree.right(null);
        }

        petrify(tree.left(), xPos - tree.offset());
        petrify(tree.right(), xPos + tree.offset());
    }
}
