package pl.stmi.algs.draw;


import pl.stmi.algs.data.Tree;

public class TreeMapper {

    public static <K> TreeNode map(Tree<K> tree) {
        if (tree == null) {
            return null;
        }

        TreeNode left = map(tree.left());
        TreeNode right = map(tree.right());

        return new TreeNode().left(left).right(right);
    }
}
