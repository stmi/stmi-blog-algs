package pl.stmi.algs;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

/**
 * Implementation of N Way Merge problem using PriorityQueue and Iterators.
 *
 * @param <T> class implementing Comparable - i.e. ordering
 */
public class NWayMerge<T extends Comparable<T>> implements Iterator<T> {

    private final PriorityQueue<Node<T>> queue = new PriorityQueue<>();

    /**
     * @param sources List of Iterators providing values of T, assumed to be sorted in ascending order.
     */
    public NWayMerge(List<Iterator<T>> sources) {
        if (sources == null) return;

        sources.stream()
                .map(Node::new)
                .filter(Node::hasNext)
                .forEach(queue::add);
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }

    @Override
    public T next() {
        if (queue.isEmpty()) {
            throw new NoSuchElementException();
        }

        Node<T> node = queue.remove();
        T returnValue = node.next();
        if (node.hasNext()) {
            queue.add(node);
        }

        return returnValue;
    }

    static class Node<T extends Comparable<T>> implements Iterator<T>, Comparable<Node<T>> {

        private final Iterator<T> source;
        private T head;
        private boolean hasNextFlag;

        public Node(Iterator<T> sourceArg) {
            this.source = sourceArg;
            advance();
        }

        @Override
        public int compareTo(Node<T> other) {
            return this.head.compareTo(other.head);
        }

        @Override
        public boolean hasNext() {
            return hasNextFlag;
        }

        @Override
        public T next() {
            T returnValue = head;
            advance();
            return returnValue;
        }

        private void advance() {
            if (source.hasNext()) {
                hasNextFlag = true;
                head = source.next();
            } else {
                hasNextFlag = false;
                head = null;
            }
        }
    }
}

