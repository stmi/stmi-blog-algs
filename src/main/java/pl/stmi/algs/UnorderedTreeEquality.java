package pl.stmi.algs;

import pl.stmi.algs.data.Tree;

import java.util.function.BiPredicate;

/**
 * Implementation of Unordered Trees equality problem
 *
 * @param <T>
 */
public class UnorderedTreeEquality<T extends Comparable<T>> implements BiPredicate<Tree<T>, Tree<T>>  {

    @Override
    public boolean test(Tree<T> first, Tree<T> second) {
        if (first == null && second == null) {
            return true;
        } else if (first == null || second == null) {
            return false;
        }

        if (!first.key().equals(second.key())) {
            return false;
        }

        Tree<T> firstSmaller = first.right();
        Tree<T> firstBigger = first.left();

        if (first.left() != null && first.right() != null) {
            if (first.left().key().compareTo(first.right().key()) < 0) {
                firstSmaller = first.left();
                firstBigger = first.right();
            }
        } else if (first.left() != null) {
            firstSmaller = first.left();
            firstBigger = first.right();
        }

        Tree<T> secondSmaller = second.right();
        Tree<T> secondBigger = second.left();

        if (second.left() != null && second.right() != null) {
            if (second.left().key().compareTo(second.right().key()) < 0) {
                secondSmaller = second.left();
                secondBigger = second.right();
            }
        } else if (second.left() != null) {
            secondSmaller = second.left();
            secondBigger = second.right();
        }

        return test(firstSmaller, secondSmaller) && test(firstBigger, secondBigger);
    }
}

